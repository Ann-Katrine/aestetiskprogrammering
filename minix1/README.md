# minix1
Her er nogle screenshots af mit program af hvordan det kan se ud når man klikker:


<img src="minix1/Skærmbillede_20230212_154156.png">
<img src="minix1/Skærmbillede_20230212_154209.png">
<img src="minix1/Skærmbillede_20230212_154234.png">
<img src="minix1/Skærmbillede_20230212_155356.png">

Linket til min kode kan jeg ikke få til at virke:( så hvis man vil se min kode må man kopiere den ind i Visual Studio og åbne den selv.

# Beskrivelse af programmet
Mit program skal forestille en masse små cirkler i forskellige farver, der rotere i hver deres bane med hver deres hastighed. I inderste bane roterer kun en cirkel, hvor der der tilføjes en cirkel hver bane der går udad fra midten. Det vil sige at der fx. er 6 cirkler der roterer i 6. bane. Alle cirkler roterer dog kun når man klikker med musen, og formålet er derved at man kan skabe et flot mønster, ved at klikke indtil man synes det ser flot ud.
# Forklaring af kode
Jeg startede med at sætte mit canvas op med en størrelse på 500x500. Dette er højde og bredde. Derudover skrev jeg også WEBGL, som egentlig muliggøre at kode i 3D da det danner en tredje dimension eller z-akse. Jeg benytter dette til at få cirklerne til at roterer rundt om z-aksen, der dannes i centrum af canvas. Baggrunden sætter jeg til 0 da den skal være sort.

~~~~
`function setup() {
createCanvas(500,500,WEBGL);
background(0);
~~~~
Dernæst bruger jeg funtionen draw(), så jeg kan komme til at tegne cirklerne. Her laver jeg også en baggrund de skal være på. Jeg skriver også noLoop, da jeg ikke vil have cirklerne til at roterer konstant.
~~~~
}
function draw() {
background(0);
noLoop();
~~~~
Første cirkel jeg har kodet er den indeste cirkel. Jeg har puttet hver "cirkelkode" ind i i en push()/pop() funktion, da dette sørger for at funktionerne for hver cirkel ikke bliver blandet sammen. Cirklen ville jeg gerne have skulle være rød, så derfor fyldte jeg den med farvekoden 255,0,0 med funktionen fill(). Jeg ville ikke have en kant rundt om cirkelen hvilket danner sig automatisk, og derfor skrev jeg noStroke().

For at  få cirklen til at rotere rundt om midten af canvas, brugte jeg functionen rotateZ(), hvilket jeg jo kunne da jeg startede koden med at tilføje denne akse. Ved at tilføje z-aksen bliver centrum sat i midten af canvas istedet for oppe i venstre hjørne. Jeg valgte at cirklen skulle rotere om z-aksen med frameCount*-0.7. frameCount er antallet af frames, der er blevet vist siden programmet startede, og når man ganger det med -0.7 skaber det altså hastigheden. Ved at det er et minustal roterer cirklen mod uret, og ved at det er et plustal roterer det med. Selve cirklen dannede jeg med funktionen ellipse(). Første værdi denne funktion er x-koordinat for cirklens centrum. Anden værdi er y-koordinat for cirklens centrum. Og de sidste to er højde og bredde på cirklen (hvis man vil have en cirkel og ikke en ellipse, er det kun nødvendigt med én værdi for cirklen radius. Dette har jeg dog ikke gjort lige her:) ).
~~~~
//rød
push();
fill(255,0,0);
noStroke();
rotateZ(frameCount*-0.7);
ellipse(0,10,15,15);
pop();
~~~~
I næste "bane" fra midten af har jeg lavet to orange cirkler. De roterer den anden vej, da de ikke har minus foran ders hastighed. Jeg har altså her bare kopieret tidligere kode, og ændret på værdierne så antallet, rotationen, placeringen og farven blev ændret.
~~~~
//orange
push();
fill(255,102,0);
noStroke();
rotateZ(frameCount*0.7);
ellipse(10,30,15,15);
pop();
push();
fill(255,102,0);
noStroke();
rotateZ(frameCount*0.6);
ellipse(10,30,15,15);
pop();
~~~~
Herunder ses næste bane med 3 pink cirkler. Disse roterer mod uret ligesom den røde cirkel. For hver cirkel der bliver tilføjet pr. bane, har jeg sænket hastigheden så cirklerne ikke ligger oven i hinanden.
~~~~
//pink
push();
fill(255,0,102);
noStroke();
rotateZ(frameCount*-0.7);
ellipse(30,50,15,15);
pop();
push();
fill(255,0,102);
noStroke();
rotateZ(frameCount*-0.6);
ellipse(30,50,15,15);
pop();
push();
fill(255,0,102);
noStroke();
rotateZ(frameCount*-0.5);
ellipse(30,50,15,15);
pop();
~~~~
Jeg har valgt kun at tage eksempler fra de først tre "baner", men der er altså hele ti baner, hvor der er blevet tilføjet en cirkel pr. bane.


Herunder ses de sidste funktioner i min kode, der indikerer det "spændende" ved koden. Funktionen mousePressed() gør nemlig at når man klikker med musen så vil funktionen under blive aktiveret. Funktionen redraw gør åbenlyst at alt under funktionen draw() bliver aktiveret igen. Ved at jeg havde tilføjet noLoop() funktionen, vil cirklerne altså kun rotere når man klikker med musen.
~~~~
}
function mousePressed() {
redraw();
}
~~~~
# Min oplevelse
Min opleplevelse med at lave denne kode var god. Jeg synes det var fedt at sidde selv og lærer kode noget ved at prøve sig frem. Jeg er positivt over kodning, og glæder mig til at lære mere. Jeg synes det var rart med en reference liste, da jeg kunne finde koder og eksempler til at lave denne kode. Jeg brugte den også til at slå mine koder op da jeg var færdig, så jeg lige kunne læse lidt om hvordan de fungerede, så jeg nemmere kunne bruge dem. Jeg synes dog det er super ærgeligt at jeg ikke kan få linket til min kode til at virke, og at den eneste måde jeg kan få koden frem på er ved at gå live i Visual Studio. Jeg har sørget for at alt er indstillet til offentligt, så jeg må få hjælp til at få det til at fungere, da jeg ikke kan gennemskue hvad der er galt.
# Reflektioner omkring programmet
Jeg synes programmet har fungeret rigtig fint. Jeg synes jeg har forstået det grundlæggende ved programmet godt, og jeg synes det er rart man kan bruge p5 referencelisten til at slå op i. Det var en god oplevelse at lave min første minix.
#Opgraderingsmuligheder
Hvis jeg skulle opgradere min kode, ville jeg gerne havde lavet det sådan så cirklerne ville roterer så længe man holdte musen nede, og så stoppe. Jeg forsøgte at kode mig frem til dette, men det lykkedes desværre ikke. Eller er jeg fint tilfreds, selvom den måske er lidt simpel, men det er også godt at starte et sted, så kan jeg udfordre mig selv lidt mere ved næste minix.
